//
//  RegisterVC.swift
//  ashal
//
//  Created by MOHAMED on 4/30/17.
//  Copyright © 2017 MOHAMED. All rights reserved.
//

import UIKit
import CoreLocation

class RegisterVC: UIViewController, CLLocationManagerDelegate {
    @IBOutlet weak var NameText: UITextField!
    @IBOutlet weak var PasswordText: UITextField!

    @IBOutlet weak var PhoneText: UITextField!
    @IBOutlet weak var RePasswordText: UITextField!
    @IBOutlet weak var EmailText: UITextField!
    var locationmanager = CLLocationManager()
    var userLocation : CLLocation!
    var locManager = CLLocationManager()
    
    var currentLocation: CLLocation!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        locManager.requestWhenInUseAuthorization()
        
        if (CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse ||
            CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways){
            currentLocation = locManager.location
            print(currentLocation.coordinate.latitude)
            print(currentLocation.coordinate.longitude)
        }
    }

    
    @IBAction func CreateAccountBu(_ sender: UIButton) {
        currentLocation = locManager.location
        let lat = currentLocation.coordinate.latitude
        let lan = currentLocation.coordinate.longitude
        guard let  name = self.NameText.text , !name.isEmpty else {return}
        guard let email = self.EmailText.text , !email.isEmpty else {return}
        guard let mobile = self.PhoneText.text , !mobile.isEmpty else {return}
        guard let password = self.PasswordText.text , !password.isEmpty else {return}
        guard let repassword = self.RePasswordText.text , !repassword.isEmpty else {return}
        
        guard password == repassword  else {return}
        
        ApiMethods.RegisterCustomer(name: name, email: email, password: password, mobile: Int(mobile), lat: lat , lan: lan){ (error , status, message) in
            if error == nil {
                if status == 1 {
                    let title:String = "تهانينا"
                    
                    let message = "تم تسجيل الحساب بنجاح"
                    
                    let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
                    
                    alert.addAction(UIAlertAction(title: "موافق", style: UIAlertActionStyle.cancel, handler:nil))
                    
                    self.present(alert, animated: true, completion: nil)
                }else {
                    let title:String = "تهانينا"
                    
                    
                    let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
                    
                    alert.addAction(UIAlertAction(title: "موافق", style: UIAlertActionStyle.cancel, handler:nil))
                    
                    self.present(alert, animated: true, completion: nil)
                }
            }
            
        }
    }


}
