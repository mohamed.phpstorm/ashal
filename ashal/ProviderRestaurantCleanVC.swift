//
//  ProviderRestaurantCleanVC.swift
//  ashal
//
//  Created by MOHAMED on 4/9/17.
//  Copyright © 2017 MOHAMED. All rights reserved.
//

import UIKit
import CoreLocation
class ProviderRestaurantCleanVC: UIViewController ,CLLocationManagerDelegate {
    
    var locationmanager = CLLocationManager()
    var userLocation : CLLocation!
    var id : Int!
    var locManager = CLLocationManager()
    
    @IBOutlet weak var PriceText: UITextField!
    var destinationLat : Double!
    var destinationLong : Double!
    var currentLocation: CLLocation!
    @IBOutlet weak var CommentText: UITextView!
    @IBOutlet weak var Clean_timeText: UILabel!
    @IBOutlet weak var Clena_dateText: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ApiMethods.GetProviderRestaurantClean{( error,id,lat,lan,clean_date ,clean_time, comment, status) in
            if status == 1 {
                self.CommentText.text = comment
                self.Clean_timeText.text = clean_time
                self.Clena_dateText.text = clean_date
                self.destinationLat = Double(lat!)
                self.destinationLong = Double(lan!)
                self.locManager.requestWhenInUseAuthorization()
                
                if (CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse ||
                    CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways){
                    self.currentLocation = self.locManager.location
                    self.id = id
                    print(self.currentLocation.coordinate.latitude)
                    print(self.currentLocation.coordinate.longitude)
                    
                    print( self.destinationLat)
                }
                
            }else {
                let title:String = "خطأ"
                
                let message = "لايوجد مهام خاصة بك"
                
                let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "موافق", style: UIAlertActionStyle.cancel, handler:nil))
                
                self.present(alert, animated: true, completion: nil)
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    
    @IBAction func ShowPlaceOnMap(_ sender: UIButton) {
        let currentLat = currentLocation.coordinate.latitude
        let currentLong = currentLocation.coordinate.longitude
        
        
        if (UIApplication.shared.canOpenURL(NSURL(string:"http://maps.apple.com")! as URL)) {
            UIApplication.shared.openURL(NSURL(string:"http://maps.apple.com/?saddr=\(currentLat),\(currentLong)&daddr=\(destinationLat),\(destinationLong)") as! URL)
        } else {
            NSLog("Can't use Apple Maps");
        }
        
    }
    
    
    
    @IBAction func AddProviderRestaurantClean(_ sender: UIButton) {
        ApiMethods.AddProviderRestaurantClean(Id: id!){ (error,status) in
            if status == 0 {
                let title:String = "خطأ"
                
                let message = "برجاء المحاولة مره أخرى"
                
                let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "موافق", style: UIAlertActionStyle.cancel, handler:nil))
                
                self.present(alert, animated: true, completion: nil)
            }else {
                let title:String = "تهانينا"
                
                let message = "تمت العملية بنجاح"
                
                let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "موافق", style: UIAlertActionStyle.cancel, handler:nil))
                
                self.present(alert, animated: true, completion: nil)
                self.dismiss(animated: true, completion: nil)
            }
            
        }
        
        
    }
    
    
}
