//
//  Constatnts.swift
//  ashal
//
//  Created by MOHAMED on 3/28/17.
//  Copyright © 2017 MOHAMED. All rights reserved.
//

import Foundation

let MainUrl = "http://arabnewtech.org/ashal/api/"

let LoginUrl = MainUrl + "login"

let RegisterUrl = MainUrl + "register"


let GetConditionerPrice = MainUrl + "conditioner/price"

let addConditionerUrl = MainUrl + "conditioner/add"

let addCarpeUrl = MainUrl + "carpet/add"


let AddHomeWaterFilterUrl = MainUrl + "home/water/add"

let getHomeWaterUnits = MainUrl + "home/water"


let HomeElectricUrl = MainUrl + "home/electric/add"

let getCleanRestaurantPrice = MainUrl + "restaurant/clean/price"

let AddCleanRestaurant = MainUrl + "restaurant/clean"

let addRestaurantElectricUrl = MainUrl + "home/electric/add"

let GetRestaurantWaterUnitsUrl = MainUrl + "restaurant/water"

let AddRestaurantWaterFilterUrl = MainUrl + "restaurant/water/add"

let WashTypesUrl = MainUrl + "cars/types"

// Provider Links
let GetProviderCarpetUrl =  MainUrl + "provider/carpet"

let AddProviderCarpetUrl = MainUrl + "provider/carpet/add/"

let GetProviderConditionerUrl = MainUrl + "provider/conditioner"
let AddProviderConditionerUrl = MainUrl + "provider/conditioner/add/"
let GetProviderWaterUrl = MainUrl + "provider/home/water"
let AddProviderWaterUrl = MainUrl + "provider/home/water/add/"
let GetProviderElectricUrl = MainUrl + "provider/home/electric"
let AddProviderElectricUrl = MainUrl + "provider/home/electric/add/"
let GetProviderRestaurantCleanUrl = MainUrl + "provider/restaurant/clean"
let AddProviderRestaurantCleanUrl = MainUrl + "provider/restaurant/clean/add/"
let GetProviderRestaurantElectricUrl = MainUrl + "provider/restaurant/electric"
let AddProviderRestaurantElectricUrl = MainUrl + "provider/restaurant/electric/"
let GetProviderRestaurantWaterUrl = MainUrl + "provider/restaurant/water"
let AddProviderRestaurantWaterUrl = MainUrl + "provider/restaurant/water/add/"
let GetProviderCarWashUrl = MainUrl + "provider/wash/car"
let AddProviderCarWashUrl = MainUrl + "provider/wash/car/add/"
let GetStaticPageUrl = MainUrl + "pages"














