//
//  HomeElectricVC.swift
//  ashal
//
//  Created by MOHAMED on 4/4/17.
//  Copyright © 2017 MOHAMED. All rights reserved.
//

import UIKit
import CoreLocation
class HomeElectricVC:UIViewController ,CLLocationManagerDelegate {
    
    
    var locationmanager = CLLocationManager()
    var userLocation : CLLocation!
    
    @IBOutlet weak var CommentText: UITextField!
    
    @IBOutlet weak var DateButton: UIButton!
    
    @IBOutlet weak var TimeButton: UIButton!
    var locManager = CLLocationManager()
    
    var currentLocation: CLLocation!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locManager.requestWhenInUseAuthorization()
        
        if (CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse ||
            CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways){
            currentLocation = locManager.location
            print(currentLocation.coordinate.latitude)
            print(currentLocation.coordinate.longitude)
        }
    }
    var birthdate: Date?
    
    @IBAction func birthdate(_ sender: UIButton) {
        self.showDatesPickerViewController { date in
            let formatter = DateFormatter()
            formatter.dateStyle = .medium
            formatter.dateFormat = "YYY-MM-dd"
            
            
            self.birthdate = date
            sender.setTitle(formatter.string(from: date), for: .normal)
        }
    }
    
    @IBAction func TimeBu(_ sender: UIButton) {
        self.ShowTimePickerController { time in
            let dateFormatter = DateFormatter()
            dateFormatter.timeStyle = .medium
            dateFormatter.dateFormat = "HH:MM"
            
            
            let timeString = dateFormatter.string(from: time)
            sender.setTitle(timeString, for: .normal)
            
        }
        
    }
    
    
    
    @IBAction func AddConditionerBu(_ sender: UIButton) {
        currentLocation = locManager.location
        let lat = currentLocation.coordinate.latitude
        let lan = currentLocation.coordinate.longitude
        if let DateString = self.DateButton.titleLabel?.text{
            let DateValue = DateString
            
            if let TimeValue = self.TimeButton.titleLabel?.text{
                
                
                
                ApiMethods.AddHomeElectric(lat: Double(lat), lan: Double(lan), clean_time: (TimeValue), clean_date: (DateValue), comment: CommentText.text!) {(error, status , message , available_time) in
                    if status == 0 {
                        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .alert)
                        for data in available_time! {
                            alert.addAction(UIAlertAction(title: data, style: UIAlertActionStyle.default, handler:{ action ->Void in
                                let buttonTitle = action.title
                                self.TimeButton.setTitle(buttonTitle, for: .normal)
                            }))
                            
                            
                        }
                        
                        
                        self.present(alert, animated: true, completion: nil)
                        alert.addAction(UIAlertAction(title: "إلغاء", style: UIAlertActionStyle.cancel, handler:nil))
                        
                        alert.popoverPresentationController?.sourceRect = (sender.bounds)
                        alert.popoverPresentationController?.sourceView = sender
                        self.navigationController?.pushViewController(alert, animated: true)
                    }
                }
            }
        }
        
    }
}
