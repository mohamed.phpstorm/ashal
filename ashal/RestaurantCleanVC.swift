//
//  RestaurantCleanVC.swift
//  ashal
//
//  Created by MOHAMED on 4/4/17.
//  Copyright © 2017 MOHAMED. All rights reserved.
//

import UIKit
import CoreLocation

class RestaurantCleanVC: UIViewController, CLLocationManagerDelegate {
    
   
    var locationmanager = CLLocationManager()
    var userLocation : CLLocation!
    var SinglePrice : Int!
    @IBOutlet weak var TotalPriceText: UILabel!
    
    @IBOutlet weak var CommentText: UITextField!
    @IBOutlet weak var NumberOfHours: UITextField!
    
    @IBOutlet weak var TimeButton: UIButton!
    @IBOutlet weak var DateButton: UIButton!
    var locManager = CLLocationManager()
    
    var currentLocation: CLLocation!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ApiMethods.GetRestaurantClean{(error,price) in
            self.SinglePrice = price
            print(self.SinglePrice)
            self.TotalPriceText.text! =  "سعر الساعة" + String(self.SinglePrice!)
            
        }
        locManager.requestWhenInUseAuthorization()
        
        if (CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse ||
            CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways){
            currentLocation = locManager.location
            print(currentLocation.coordinate.latitude)
            print(currentLocation.coordinate.longitude)
        }
        NumberOfHours.addTarget(self, action: #selector(SetAllPrices), for: .editingChanged)
        // Do any additional setup after loading the view.
    }
    var birthdate: Date?
    
    @IBAction func birthdate(_ sender: UIButton) {
        self.showDatesPickerViewController { date in
            let formatter = DateFormatter()
            formatter.dateStyle = .medium
            formatter.dateFormat = "YYY-MM-dd"
            
            
            self.birthdate = date
            sender.setTitle(formatter.string(from: date), for: .normal)
        }
    }
    @objc func SetAllPrices(){
        if self.SinglePrice != nil && !(self.NumberOfHours.text?.isEmpty)!{
            self.TotalPriceText.text = String(Int(NumberOfHours.text!)! * self.SinglePrice!)
            
        }
    }
    @IBAction func TimeBu(_ sender: UIButton) {
        self.ShowTimePickerController { time in
            let dateFormatter = DateFormatter()
            dateFormatter.timeStyle = .medium
            dateFormatter.dateFormat = "HH:MM"
            
            
            let timeString = dateFormatter.string(from: time)
            sender.setTitle(timeString, for: .normal)
            
        }
        
    }
    
    @IBAction func AddRestaurantCleanBu(_ sender: UIButton) {
        currentLocation = locManager.location
        let lat = currentLocation.coordinate.latitude
        let lan = currentLocation.coordinate.longitude
        if let DateString = self.DateButton.titleLabel?.text{
            let DateValue = DateString
            
            if let TimeValue = self.TimeButton.titleLabel?.text{
                
                ApiMethods.AddRestaurantClean(lat: Double(lat), lan: Double(lan), units: Int(NumberOfHours.text!)!, clean_time: (TimeValue), price: Int(TotalPriceText.text!)!, clean_date: (DateValue), comment: CommentText.text!) {(error, status , message , available_time) in
                    if status == 0 {
                        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .alert)
                        for data in available_time! {
                            alert.addAction(UIAlertAction(title: data, style: UIAlertActionStyle.default, handler:{ action ->Void in
                                let buttonTitle = action.title
                                self.TimeButton.setTitle(buttonTitle, for: .normal)
                            }))
                            
                            
                        }
                        
                        
                        self.present(alert, animated: true, completion: nil)
                        alert.addAction(UIAlertAction(title: "إلغاء", style: UIAlertActionStyle.cancel, handler:nil))
                        
                        alert.popoverPresentationController?.sourceRect = (sender.bounds)
                        alert.popoverPresentationController?.sourceView = sender
                        self.navigationController?.pushViewController(alert, animated: true)
                    }
                }
            }
        }
        
    }
}














