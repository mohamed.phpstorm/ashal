//
//  HomeVC.swift
//  ashal
//
//  Created by MOHAMED on 4/11/17.
//  Copyright © 2017 MOHAMED. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import Firebase
import GoogleSignIn
import TwitterKit
import Fabric
import CoreLocation
class HomeVC: UITableViewController, FBSDKLoginButtonDelegate , GIDSignInUIDelegate, CLLocationManagerDelegate{
    
    @IBOutlet weak var TwitterBu: UIButton!
    @IBOutlet weak var HomeCleanButton: UIButton!
    
    @IBOutlet weak var HomeWaterFilterButton: UIButton!
    @IBOutlet weak var HomeElectricButton: UIButton!
    @IBOutlet weak var FaceBookLoginBu: UIButton!
    
    @IBOutlet weak var WaterFilterButton: UIButton!
    @IBOutlet weak var RestaurantElectricButton: UIButton!
    @IBOutlet weak var RestaurantCleanButton: UIButton!
    @IBOutlet weak var OilButton: UIButton!
    @IBOutlet weak var WashButton: UIButton!
    @IBOutlet weak var GooglePlusBu: UIButton!
    var locationmanager = CLLocationManager()
    var userLocation : CLLocation!
    var locManager = CLLocationManager()
    
    var currentLocation: CLLocation!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.WaterFilterButton.titleLabel?.adjustsFontSizeToFitWidth = true
        self.HomeCleanButton.titleLabel?.adjustsFontSizeToFitWidth = true
        self.HomeWaterFilterButton.titleLabel?.adjustsFontSizeToFitWidth = true
        self.HomeElectricButton.titleLabel?.adjustsFontSizeToFitWidth = true
        self.RestaurantElectricButton.titleLabel?.adjustsFontSizeToFitWidth = true
        self.RestaurantCleanButton.titleLabel?.adjustsFontSizeToFitWidth = true
        self.OilButton.titleLabel?.adjustsFontSizeToFitWidth = true
        self.WashButton.titleLabel?.adjustsFontSizeToFitWidth = true
        if (CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse ||
            CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways){
            currentLocation = locManager.location
            print(currentLocation.coordinate.latitude)
            print(currentLocation.coordinate.longitude)
        }
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        if (FBSDKAccessToken.current() == nil ) {
            print("Not Login")
        } else {
            print("login")
        }
        
        let loginBu = FBSDKLoginButton()
        loginBu.delegate = self
        FaceBookLoginBu.addTarget(self, action: #selector(FaceBookLoginFunc), for: .touchUpInside)
        TwitterBu.addTarget(self, action: #selector(LoginWithTwitter), for: .touchUpInside)
        GooglePlusBu.addTarget(self, action: #selector(LoginWithGplus), for: .touchUpInside)
        
        
        
        GIDSignIn.sharedInstance().uiDelegate = self
        // view.addSubview(TwitterLogin)
        
    }
    
    func FaceBookLoginFunc(){
        FBSDKLoginManager().logIn(withReadPermissions: ["public_profile","email",], from: self) { (result , err) in
            if (result?.isCancelled)! {
                return
            } else {
                if err == nil {
                    
                    self.currentLocation = self.locManager.location
                    let lat = self.currentLocation.coordinate.latitude
                    let lan = self.currentLocation.coordinate.longitude
                    let token = result?.token.tokenString
                    self.showEmailAddress { (error, id, name) in
                        ApiMethods.RegisterCustomer(name: name, email: id! + "@facebook.com", password: token, mobile: Int(id!), lat: lat, lan: lan) { (error , status , message) in
                            
                            if error == nil {
                                if status == 1 {
                                    let title:String = "تهانينا"
                                    
                                    let message = "تم تسجيل الحساب بنجاح"
                                    
                                    let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
                                    alert.addAction(UIAlertAction(title: "موافق", style: UIAlertActionStyle.cancel, handler:nil))
                                    
                                    self.present(alert, animated: true, completion: nil)
                                }else {
                                    let title:String = "تهانينا"
                                    
                                    
                                    let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
                                    
                                    alert.addAction(UIAlertAction(title: "موافق", style: UIAlertActionStyle.cancel, handler:nil))
                                    
                                    self.present(alert, animated: true, completion: nil)
                                }
                            }
                        }
                        
                    }
                    
                }
            }
            
        }
    }
    
    
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        if error != nil {
            print(error)
            return
        }
        
        
    }
    func showEmailAddress(compltion : @escaping (_ error:Error?,_ id : String?,  _ name : String?)->Void) {
        FBSDKGraphRequest(graphPath: "/me", parameters: ["fields": "id,email,name"]).start { (connectione, result, err) in
            
            if err != nil {
                print(err)
                return compltion(err, nil , nil)
            }else {
                
            }
            let data = result as! [String:Any]
            let id =  data["id"]
            let name = data["name"]
            print(id)
            print(name)
            
            
            return compltion(nil, id as? String, name as! String)
        }
    }
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        print("logout")
    }
    
    // Login With Twitter
    func LoginWithTwitter(){
        Twitter.sharedInstance().logIn { (session, error) in
            if let err = error {
                print("Failed " , err)
            }else {
                let id =  session?.userID
                let name = session?.userName
                
                let token = session?.authTokenSecret
                self.currentLocation = self.locManager.location
                let lat = self.currentLocation.coordinate.latitude
                let lan = self.currentLocation.coordinate.longitude
                ApiMethods.RegisterCustomer(name: name, email: id! + "@twitter.com", password: token, mobile: Int(id!), lat: lat, lan: lan){ (error , status , message) in
                    
                    if error == nil {
                        if status == 1 {
                            let title:String = "تهانينا"
                            
                            let message = "تم تسجيل الحساب بنجاح"
                            
                            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
                            
                            alert.addAction(UIAlertAction(title: "موافق", style: UIAlertActionStyle.cancel, handler:nil))
                            
                            self.present(alert, animated: true, completion: nil)
                        }else {
                            let title:String = "تهانينا"
                            
                            
                            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
                            
                            alert.addAction(UIAlertAction(title: "موافق", style: UIAlertActionStyle.cancel, handler:nil))
                            
                            self.present(alert, animated: true, completion: nil)
                        }
                        
                    }
                }
            }
            
        }
        
        
    }
    // Login With Gplus
    func LoginWithGplus() {
        GIDSignIn.sharedInstance().signIn()
    }
    
    func signIn(signIn: GIDSignIn!,
                presentViewController viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
    }
    func signIn(signIn: GIDSignIn!,
                dismissViewController viewController: UIViewController!) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func signIn(signIn: GIDSignIn!, didSignInForUser user: GIDGoogleUser!,
                withError error: NSError!) {
        if (error == nil) {
            // Perform any operations on signed in user here.
            let userId = user.userID                  // For client-side use only!
            let idToken = user.authentication.idToken // Safe to send to the server
            let fullName = user.profile.name
            let givenName = user.profile.givenName
            let familyName = user.profile.familyName
            let email = user.profile.email
            // ...
            self.currentLocation = self.locManager.location
            let lat = self.currentLocation.coordinate.latitude
            let lan = self.currentLocation.coordinate.longitude
            ApiMethods.RegisterCustomer(name: fullName, email: email, password: idToken, mobile: Int(userId!), lat: lat, lan: lan) { (error , status , message) in
                
                if error == nil {
                    if status == 1 {
                        let title:String = "تهانينا"
                        
                        let message = "تم تسجيل الحساب بنجاح"
                        
                        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
                        
                        alert.addAction(UIAlertAction(title: "موافق", style: UIAlertActionStyle.cancel, handler:nil))
                        
                        self.present(alert, animated: true, completion: nil)
                    }else {
                        let title:String = "تهانينا"
                        
                        
                        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
                        
                        alert.addAction(UIAlertAction(title: "موافق", style: UIAlertActionStyle.cancel, handler:nil))
                        
                        self.present(alert, animated: true, completion: nil)
                    }
                    
                }
            }
        } else {
            print("\(error.localizedDescription)")
        }
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
