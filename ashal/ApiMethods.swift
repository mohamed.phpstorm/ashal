//
//  ApiMethods.swift
//  ashal
//
//  Created by MOHAMED on 3/28/17.
//  Copyright © 2017 MOHAMED. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ApiMethods {
    
    
    
    
    // Register
    class func RegisterCustomer(name: String! , email : String! , password : String! , mobile : Int?,lat:Double, lan : Double,compltion : @escaping (_ error:Error?,_ status : Int?, _ message : String)->Void){
        let url = URL(string: RegisterUrl)!
        let parameters = [
            "name": name ,
            "email" : email,
            "password" : password ,
            "phone" : mobile,
            "lat" : lat,
            "lan" : lan,
            "address" : "test"
            ] as [String : Any]
        Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            switch response.result {
            case .failure(let error) :
                
                compltion(error, 0,"حطأ")
                print(error)
                return
            case .success(let value):
                let json = JSON(value)
                let message = json["msg"].string
                compltion(nil, 1, message!)
                return
            }
            
        }
    }
    class func LoginCustomer ( email : String , password : String ,compltion : @escaping (_ error:Error?,_ status : Int?, _ message : String)->Void) {
        let url = URL(string: LoginUrl)!
        let parameters = [
            "email" : email ,
            "password" : password
        ]
        Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            switch response.result {
            case .failure(let error) :
                
                compltion(error, nil, "خطأ")
                return
                
            case .success(let value):
                let json = JSON(value)
                
            }
        }
    }
    // Get getConditionerPrice
    class func getConditionerPrice(compltion : @escaping (_ error:Error?,_ price : Int?)->Void){
        
        let api_token = "TNYXjwlJkPoytNVUj2hgf8UVuswtLzzz75pBg5mw9m9qNl29DQ9HvBFbi3Q6"
        let parameters = [
            "api_token":api_token
        ]
        let url = URL(string: GetConditionerPrice)!
        
        Alamofire.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            switch response.result {
            case .failure(let error):
                compltion(error, nil)
                print(error)
                return
            case .success(let value):
                let json = JSON(value)
                if let price = json["price"].int {
                    compltion(nil, price)
                    print(price)
                }
                
                return
                
            }
        }
    }
    
    // Add Conditioner
    class func AddConditioner(api_token : String,lat:Double , lan:Double, units : Int , price : Int,clean_time: String, clean_date: String , comment: String,compltion : @escaping (_ error:Error?,_ staus:Int?,_ messge:String?,_ available_time : [String]?)->Void){
        let parametes = [
            "api_token" : api_token,
            "lat" : lat ,
            "lan" : lan,
            "units": units,
            "price" : price,
            "clean_date": clean_date,
            "clean_time" : clean_time,
            "comment" : comment
            ] as [String : Any]
        let url = URL(string: addConditionerUrl)!
        Alamofire.request(url, method: .post, parameters: parametes, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            switch response.result {
            case .failure(let error):
                print(error)
                compltion(error, nil, nil, nil)
                return
            case .success(let value):
                print(value)
                let json = JSON(value)
                if  let status = json["status"].int{
                    if let message = json["msg"].string{
                        if let data: [String] = json["data"].arrayObject as! [String]?{
                            compltion(nil, status, message, data)
                        }
                    }
                }
            }
        }
    }
    
    
    // Add Carpet
    class func AddCarpet (api_token : String,lat:Double , lan:Double, units : Int ,clean_time: String, clean_date: String , comment: String,compltion : @escaping (_ error:Error?,_ staus:Int?,_ messge:String?,_ available_time : [String]?)->Void){
        let parametes = [
            "api_token" : api_token,
            "lat" : lat ,
            "lan" : lan,
            "units": units,
            "clean_date": clean_date,
            "clean_time" : clean_time,
            "comment" : comment
            ] as [String : Any]
        let url = URL(string: addCarpeUrl)!
        Alamofire.request(url, method: .post, parameters: parametes, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            switch response.result {
            case .failure(let error):
                print(error)
                compltion(error, nil, nil, nil)
                return
            case .success(let value):
                print(value)
                let json = JSON(value)
                if  let status = json["status"].int{
                    if let message = json["msg"].string{
                        if let data: [String] = json["data"].arrayObject as! [String]?{
                            compltion(nil, status, message, data)
                        }
                    }
                }
            }
        }
        
    }
    
    
    
    // Get Homw Water Filter
    class func GetHomeWaterUnits(api_token : String,compltion : @escaping (_ error:Error?,_ waterUnits : [Dictionary <String,Any>]?)->Void){
        let parameters = [
            "api_token" : api_token
        ]
        let url = URL(string: getHomeWaterUnits)!
        Alamofire.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            switch response.result {
            case .failure(let error):
                compltion(error, nil)
                print(error)
            case .success(let value):
                let json = JSON(value)
                if  let data:[Dictionary <String,Any>] = json["data"].arrayObject as? [Dictionary <String,Any>]{
                    compltion(nil, data)
                }
                
            }
        }
    }
    
    // Add Home Water Filter
    
    class func AddHomeWater (api_token : String,lat:Double , lan:Double, units : Int ,clean_time: String, price:Int,clean_date: String , comment: String,compltion : @escaping (_ error:Error?,_ staus:Int?,_ messge:String?,_ available_time : [String]?)->Void){
        let parametes = [
            "api_token" : api_token,
            "lat" : lat ,
            "lan" : lan,
            "units": units,
            "clean_date": clean_date,
            "clean_time" : clean_time,
            "price" :price,
            "comment" : comment
            ] as [String : Any]
        let url = URL(string: AddHomeWaterFilterUrl)!
        Alamofire.request(url, method: .post, parameters: parametes, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            switch response.result {
            case .failure(let error):
                print(error)
                compltion(error, nil, nil, nil)
                return
            case .success(let value):
                print(value)
                let json = JSON(value)
                if  let status = json["status"].int{
                    if let message = json["msg"].string{
                        if let data: [String] = json["data"].arrayObject as! [String]?{
                            compltion(nil, status, message, data)
                        }
                    }
                }
            }
        }
        
    }
    
    
    class func AddHomeElectric(api_token : String,lat:Double , lan:Double ,clean_time: String,clean_date: String , comment: String,compltion : @escaping (_ error:Error?,_ staus:Int?,_ messge:String?,_ available_time : [String]?)->Void){
        let parametes = [
            "api_token" : api_token,
            "lat" : lat ,
            "lan" : lan,
            "clean_date": clean_date,
            "clean_time" : clean_time,
            "comment" : comment
            ] as [String : Any]
        let url = URL(string: HomeElectricUrl)!
        Alamofire.request(url, method: .post, parameters: parametes, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            switch response.result {
            case .failure(let error):
                print(error)
                compltion(error, nil, nil, nil)
                return
            case .success(let value):
                print(value)
                let json = JSON(value)
                if  let status = json["status"].int{
                    if let message = json["msg"].string{
                        if let data: [String] = json["data"].arrayObject as! [String]?{
                            compltion(nil, status, message, data)
                        }
                    }
                }
            }
        }
    }
    class func GetRestaurantClean(api_token : String,compltion : @escaping (_ error:Error?,_ CleanPrice : Int?)->Void){
        let parameters = [
            "api_token" : api_token
        ]
        let url = URL(string: getCleanRestaurantPrice)!
        Alamofire.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            switch response.result {
            case .failure(let error):
                compltion(error, nil)
                print(error)
            case .success(let value):
                let json = JSON(value)
                let data = json["data"]
                if let price = data["price"].int {
                    compltion(nil, price)
                }
                
            }
        }
    }
    
    
    // Add Restaurant Clean
    
    
    class func AddRestaurantClean(api_token : String,lat:Double , lan:Double, units : Int ,clean_time: String, price:Int,clean_date: String , comment: String,compltion : @escaping (_ error:Error?,_ staus:Int?,_ messge:String?,_ available_time : [String]?)->Void){
        let parametes = [
            "api_token" : api_token,
            "lat" : lat ,
            "lan" : lan,
            "hours": units,
            "clean_date": clean_date,
            "clean_time" : clean_time,
            "price" :price,
            "comment" : comment
            ] as [String : Any]
        let url = URL(string: AddCleanRestaurant)!
        Alamofire.request(url, method: .post, parameters: parametes, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            switch response.result {
            case .failure(let error):
                print(error)
                compltion(error, nil, nil, nil)
                return
            case .success(let value):
                print(value)
                let json = JSON(value)
                if  let status = json["status"].int{
                    if let message = json["msg"].string{
                        if let data: [String] = json["data"].arrayObject as! [String]?{
                            compltion(nil, status, message, data)
                        }
                    }
                }
            }
        }
        
    }
    
    // Add Restaurant Electric Func
    
    class func addRestaurantElectric(api_token : String,lat:Double , lan:Double ,clean_time: String,clean_date: String , comment: String,compltion : @escaping (_ error:Error?,_ staus:Int?,_ messge:String?,_ available_time : [String]?)->Void){
        let parametes = [
            "api_token" : api_token,
            "lat" : lat ,
            "lan" : lan,
            "clean_date": clean_date,
            "clean_time" : clean_time,
            "comment" : comment
            ] as [String : Any]
        let url = URL(string: addRestaurantElectricUrl)!
        Alamofire.request(url, method: .post, parameters: parametes, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            switch response.result {
            case .failure(let error):
                print(error)
                compltion(error, nil, nil, nil)
                return
            case .success(let value):
                print(value)
                let json = JSON(value)
                if  let status = json["status"].int{
                    if let message = json["msg"].string{
                        if let data: [String] = json["data"].arrayObject as! [String]?{
                            compltion(nil, status, message, data)
                        }
                    }
                }
            }
        }
    }
    
    
    
    // Water Filter For Restaurant
    class func GetReataurantWaterUnits(api_token : String,compltion : @escaping (_ error:Error?,_ waterUnits : [Dictionary <String,Any>]?)->Void){
        let parameters = [
            "api_token" : api_token
        ]
        let url = URL(string: GetRestaurantWaterUnitsUrl)!
        Alamofire.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            switch response.result {
            case .failure(let error):
                compltion(error, nil)
                print(error)
            case .success(let value):
                let json = JSON(value)
                if  let data:[Dictionary <String,Any>] = json["data"].arrayObject as? [Dictionary <String,Any>]{
                    compltion(nil, data)
                }
                
            }
        }
    }
    
    // Add Water Filter Restaurant
    
    
    class func AddRestaurantWaterFilter (api_token : String,lat:Double , lan:Double, units : Int ,clean_time: String, price:Int,clean_date: String , comment: String,compltion : @escaping (_ error:Error?,_ staus:Int?,_ messge:String?,_ available_time : [String]?)->Void){
        let parametes = [
            "api_token" : api_token,
            "lat" : lat ,
            "lan" : lan,
            "units": units,
            "clean_date": clean_date,
            "clean_time" : clean_time,
            "price" :price,
            "comment" : comment
            ] as [String : Any]
        let url = URL(string: AddRestaurantWaterFilterUrl)!
        Alamofire.request(url, method: .post, parameters: parametes, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            switch response.result {
            case .failure(let error):
                print(error)
                compltion(error, nil, nil, nil)
                return
            case .success(let value):
                print(value)
                let json = JSON(value)
                if  let status = json["status"].int{
                    if let message = json["msg"].string{
                        if let data: [String] = json["data"].arrayObject as! [String]?{
                            compltion(nil, status, message, data)
                        }
                    }
                }
            }
        }
        
    }
    
    class func WashTypes(api_token : String,compltion : @escaping (_ error:Error?,_ washTypes : [Dictionary <String,Any>]?)->Void){
        let parameters = [
            "api_token" : api_token
        ]
        let url = URL(string: WashTypesUrl)!
        Alamofire.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            switch response.result {
            case .failure(let error):
                compltion(error, nil)
                print(error)
            case .success(let value):
                let json = JSON(value)
                if  let data:[Dictionary <String,Any>] = json["data"].arrayObject as? [Dictionary <String,Any>]{
                    print(data)
                    compltion(nil, data)
                }
                
            }
        }
    }
    
    /// Provider Methods
    // Get All Carpet Tasks
    class func GetProviderCarpet(api_token : String,compltion : @escaping (_ error:Error?,_ id:Int? ,_ lat:Double?  , _ lan : Double?, _ clean_date : String? , _ clean_time : String? , _ comment : String?, _ status : Int?)->Void){
        let parameters = [
            "api_token" : api_token
        ]
        let url = URL(string: GetProviderCarpetUrl)!
        Alamofire.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            switch response.result {
            case .failure(let error):
                compltion(error, nil, nil, nil, nil, nil,nil, nil)
                print(error)
                return
            case .success(let value):
                let json = JSON(value)
                let provider = json["provider"]
                let comment = provider["comment"].string
                let lat = provider["lat"].double
                let lan = provider["lan"].double
                let clean_date = provider["clean_date"].string
                let clean_time = provider["clean_time"].string
                let id = provider["id"].int
                let status = json["status"].int
                
                compltion(nil, id,lat, lan, clean_date, clean_time, comment, status)
                return
                
            }
        }
    }
    
    // Add Provider Carpet
    class func AddProviderCarpet(api_token : String,price : Int , Id : Int,compltion : @escaping (_ error:Error?,_ status : Int?)->Void){
        
        let parameters = [
            "api_token" : api_token,
            "price" : price
            ] as [String : Any]
        let url = URL(string: AddProviderCarpetUrl + String(Id))!
        Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            switch response.result {
            case .failure(let error):
                print(error)
                compltion(error,nil)
            case .success(let value):
                let json = JSON(value)
                let status = json["status"].int
                compltion(nil, status)
                return
            }
        }
        
    }
    // Get provider Conditioner
    
    class func GetProviderConditioner(api_token : String,compltion : @escaping (_ error:Error?,_ id:Int? ,_ lat:Double?  , _ lan : Double?, _ clean_date : String? , _ clean_time : String? , _ comment : String?, _ status : Int?)->Void){
        let parameters = [
            "api_token" : api_token
        ]
        let url = URL(string: GetProviderConditionerUrl)!
        Alamofire.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            switch response.result {
            case .failure(let error):
                compltion(error, nil, nil, nil, nil, nil,nil,nil)
                print(error)
                return
            case .success(let value):
                let json = JSON(value)
                let provider = json["provider"]
                let comment = provider["comment"].string
                let lat = provider["lat"].double
                let lan = provider["lan"].double
                let clean_date = provider["clean_date"].string
                let clean_time = provider["clean_time"].string
                let id = provider["id"].int
                let status = json["status"].int
                
                compltion(nil, id,lat, lan, clean_date, clean_time, comment,status)
                return
                
            }
        }
    }
    // Add Provider Conditioner
    class func AddProviderConditioner( api_token : String,Id : Int,compltion : @escaping (_ error:Error?,_ status : Int?)->Void){
        
        let parameters = [
            "api_token" : api_token,
            ] as [String : Any]
        let url = URL(string: AddProviderConditionerUrl + String(Id))!
        Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            switch response.result {
            case .failure(let error):
                print(error)
                compltion(error,nil)
            case .success(let value):
                let json = JSON(value)
                let status = json["status"].int
                compltion(nil, status)
                return
            }
        }
    }
    
    // Get Provider Electric
    class func GetProviderElectric(api_token : String,compltion : @escaping (_ error:Error?,_ id:Int? ,_ lat:Double?  , _ lan : Double?, _ clean_date : String? , _ clean_time : String? , _ comment : String?, _ status : Int?)->Void){
        let parameters = [
            "api_token" : api_token
        ]
        let url = URL(string: GetProviderElectricUrl)!
        Alamofire.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            switch response.result {
            case .failure(let error):
                compltion(error, nil, nil, nil, nil, nil,nil, nil)
                print(error)
                return
            case .success(let value):
                let json = JSON(value)
                let provider = json["provider"]
                let comment = provider["comment"].string
                let lat = provider["lat"].double
                let lan = provider["lan"].double
                let clean_date = provider["clean_date"].string
                let clean_time = provider["clean_time"].string
                let id = provider["id"].int
                let status = json["status"].int
                
                compltion(nil, id,lat, lan, clean_date, clean_time, comment, status)
                return
                
            }
        }
    }
    // Add Provider Electric
    class func AddProviderElectric(api_token : String,price : Int , Id : Int,compltion : @escaping (_ error:Error?,_ status : Int?)->Void){
        
        let parameters = [
            "api_token" : api_token,
            "price" : price
            ] as [String : Any]
        let url = URL(string: AddProviderElectricUrl + String(Id))!
        Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            switch response.result {
            case .failure(let error):
                print(error)
                compltion(error,nil)
            case .success(let value):
                let json = JSON(value)
                let status = json["status"].int
                compltion(nil, status)
                return
            }
        }
        
    }
    
    // Get Provider Water
    class func GetProviderWater(api_token : String,compltion : @escaping (_ error:Error?,_ id:Int? ,_ lat:Double?  , _ lan : Double?, _ clean_date : String? , _ clean_time : String? , _ comment : String?, _ status : Int?)->Void){
        let parameters = [
            "api_token" : api_token
        ]
        let url = URL(string: GetProviderWaterUrl)!
        Alamofire.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            switch response.result {
            case .failure(let error):
                compltion(error, nil, nil, nil, nil, nil,nil,nil)
                print(error)
                return
            case .success(let value):
                let json = JSON(value)
                let provider = json["provider"]
                let comment = provider["comment"].string
                let lat = provider["lat"].double
                let lan = provider["lan"].double
                let clean_date = provider["clean_date"].string
                let clean_time = provider["clean_time"].string
                let id = provider["id"].int
                let status = json["status"].int
                
                compltion(nil, id,lat, lan, clean_date, clean_time, comment,status)
                return
                
            }
        }
    }
    // Add Provider Water
    class func AddProviderWater(api_token : String,Id : Int,compltion : @escaping (_ error:Error?,_ status : Int?)->Void){
        
        let parameters = [
            "api_token" : api_token,
            ] as [String : Any]
        let url = URL(string: AddProviderWaterUrl + String(Id))!
        Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            switch response.result {
            case .failure(let error):
                print(error)
                compltion(error,nil)
            case .success(let value):
                let json = JSON(value)
                let status = json["status"].int
                compltion(nil, status)
                return
            }
        }
    }
    // Get Restaurant Clean
    class func GetProviderRestaurantClean(api_token : String,compltion : @escaping (_ error:Error?,_ id:Int? ,_ lat:Double?  , _ lan : Double?, _ clean_date : String? , _ clean_time : String? , _ comment : String?, _ status : Int?)->Void){
        let parameters = [
            "api_token" : api_token
        ]
        let url = URL(string: GetProviderRestaurantCleanUrl)!
        Alamofire.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            switch response.result {
            case .failure(let error):
                compltion(error, nil, nil, nil, nil, nil,nil,nil)
                print(error)
                return
            case .success(let value):
                let json = JSON(value)
                let provider = json["provider"]
                let comment = provider["comment"].string
                let lat = provider["lat"].double
                let lan = provider["lan"].double
                let clean_date = provider["clean_date"].string
                let clean_time = provider["clean_time"].string
                let id = provider["id"].int
                let status = json["status"].int
                
                compltion(nil, id,lat, lan, clean_date, clean_time, comment,status)
                return
                
            }
        }
    }
    // Add Provider restaurant Clean
    class func AddProviderRestaurantClean( api_token : String,Id : Int,compltion : @escaping (_ error:Error?,_ status : Int?)->Void){
        
        let parameters = [
            "api_token" : api_token,
            
            ] as [String : Any]
        let url = URL(string: AddProviderRestaurantCleanUrl + String(Id))!
        Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            switch response.result {
            case .failure(let error):
                print(error)
                compltion(error,nil)
            case .success(let value):
                let json = JSON(value)
                let status = json["status"].int
                compltion(nil, status)
                return
            }
        }
    }
    // Get Provider Restaurant Electric
    
    class func GetProviderRestarantElectric(api_token : String,compltion : @escaping (_ error:Error?,_ id:Int? ,_ lat:Double?  , _ lan : Double?, _ clean_date : String? , _ clean_time : String? , _ comment : String?, _ status : Int?)->Void){
        let parameters = [
            "api_token" : api_token
        ]
        let url = URL(string: GetProviderRestaurantElectricUrl)!
        Alamofire.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            switch response.result {
            case .failure(let error):
                compltion(error, nil, nil, nil, nil, nil,nil, nil)
                print(error)
                return
            case .success(let value):
                let json = JSON(value)
                let provider = json["provider"]
                let comment = provider["comment"].string
                let lat = provider["lat"].double
                let lan = provider["lan"].double
                let clean_date = provider["clean_date"].string
                let clean_time = provider["clean_time"].string
                let id = provider["id"].int
                let status = json["status"].int
                
                compltion(nil, id,lat, lan, clean_date, clean_time, comment, status)
                return
                
            }
        }
    }
    
    // Add Provider Restaurant Electric
    class func AddProviderRestaurantElectric(api_token : String,price : Int , Id : Int,compltion : @escaping (_ error:Error?,_ status : Int?)->Void){
        
        let parameters = [
            "api_token" : api_token,
            "price" : price
            ] as [String : Any]
        let url = URL(string: AddProviderRestaurantElectricUrl + String(Id))!
        Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            switch response.result {
            case .failure(let error):
                print(error)
                compltion(error,nil)
            case .success(let value):
                let json = JSON(value)
                let status = json["status"].int
                compltion(nil, status)
                return
            }
        }
        
    }
    
    // Get Provider Restaurant Water
    class func GetProviderRestaurantWater(api_token : String,compltion : @escaping (_ error:Error?,_ id:Int? ,_ lat:Double?  , _ lan : Double?, _ clean_date : String? , _ clean_time : String? , _ comment : String?, _ status : Int?)->Void){
        let parameters = [
            "api_token" : api_token
        ]
        let url = URL(string: GetProviderRestaurantWaterUrl)!
        Alamofire.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            switch response.result {
            case .failure(let error):
                compltion(error, nil, nil, nil, nil, nil,nil,nil)
                print(error)
                return
            case .success(let value):
                let json = JSON(value)
                let provider = json["provider"]
                let comment = provider["comment"].string
                let lat = provider["lat"].double
                let lan = provider["lan"].double
                let clean_date = provider["clean_date"].string
                let clean_time = provider["clean_time"].string
                let id = provider["id"].int
                let status = json["status"].int
                
                compltion(nil, id,lat, lan, clean_date, clean_time, comment,status)
                return
                
            }
        }
    }
    // Add Provider Restaurant Water
    class func AddProviderRestaurantWater(Id : Int,compltion : @escaping (_ error:Error?,_ status : Int?)->Void){
        
        let api_token = "eZoGcrRjoKERBybt4VocB1SBCoaL2bUT5MAqdTqXfdNg7kfPuvYMirEE381e"
        let parameters = [
            "api_token" : api_token,
            ] as [String : Any]
        let url = URL(string: AddProviderRestaurantWaterUrl + String(Id))!
        Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            switch response.result {
            case .failure(let error):
                print(error)
                compltion(error,nil)
            case .success(let value):
                let json = JSON(value)
                let status = json["status"].int
                compltion(nil, status)
                return
            }
        }
    }
    // Get Provider Car Wash
    class func GetProviderCarWash(api_token : String,compltion : @escaping (_ error:Error?,_ id:Int? ,_ lat:Double?  , _ lan : Double?, _ clean_date : String? , _ clean_time : String? , _ comment : String?, _ status : Int?)->Void){
        let parameters = [
            "api_token" : api_token
        ]
        let url = URL(string: GetProviderCarWashUrl)!
        Alamofire.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            switch response.result {
            case .failure(let error):
                compltion(error, nil, nil, nil, nil, nil,nil,nil)
                print(error)
                return
            case .success(let value):
                let json = JSON(value)
                let provider = json["provider"]
                let comment = provider["comment"].string
                let lat = provider["lat"].double
                let lan = provider["lan"].double
                let clean_date = provider["clean_date"].string
                let clean_time = provider["clean_time"].string
                let id = provider["id"].int
                let status = json["status"].int
                
                compltion(nil, id,lat, lan, clean_date, clean_time, comment,status)
                return
                
            }
        }
    }
    
    class func AddProviderCarWash(api_token : String,Id : Int,compltion : @escaping (_ error:Error?,_ status : Int?)->Void){
        
        let parameters = [
            "api_token" : api_token,
            ] as [String : Any]
        let url = URL(string: AddProviderRestaurantWaterUrl + String(Id))!
        Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            switch response.result {
            case .failure(let error):
                print(error)
                compltion(error,nil)
            case .success(let value):
                let json = JSON(value)
                let status = json["status"].int
                compltion(nil, status)
                return
            }
        }
    }
    class func GetPages(Id : Int,compltion : @escaping (_ error:Error?,_ content : String?,_ title : String?)->Void){
        
        
        let url = URL(string: GetStaticPageUrl + String(Id))!
        Alamofire.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            switch response.result {
            case .failure(let error) :
                print(error)
                compltion(error, nil, nil)
                return
            case .success(let value):
                let json = JSON(value)
                let data = json["data"]
                let title = data["arabic_title"].string
                let content = data["arabic_content"].string
                compltion(nil, content, title)
                return
                
            }
        }
    }
}


